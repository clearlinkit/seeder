## Overview

The Clearlink Seeder adds functionality to Laravel 5.0+, allowing you to seed database tables with .csv files across different environments and on different database connections.

## Requirements

#### Database User

The Clearlink Seeder **requires** the MySQL user used for the database connection to have the following permissions:

- DROP tables *(to perform truncates)*
- FILE privilege *(to load data from a local infile)*

#### Directory Structure

Your project's /database/seeds directory should be stucture as:  
`/database/seeds/{environment}/{database connection}/{filename}.csv`

`{environment}` - the environment in which this .csv will seed  
`{database connection}` - the name of the database connection to use while seeding  
`{filename}` - the name of the .csv file, which includes a timestamp and the name of the table to seed

**Example:** `/database/seeds/stage/common/20170107083000-users.csv`  
*The above .csv would seed a table named: users in the environment: stage on a database connection named: common*

#### Filenames

Your .csv files must be named like a Laravel migration file:  YmdHis-table_name.csv  
**Example:**  *20170107083000-users.csv*

## Installation

Step 1: In your project's composer.json file, add the repository:  
    ```
    "repositories": [  
        {  
            "type":"vcs",  
            "url":"https://bitbucket.org/clearlinkit/seeder.git"  
        }  
    ]
    ```

Step 2: In your project's composer.json file, add the project:  
`"clearlink/seeder": "dev-master"`

Step 3: In your project's config/app.php, add the service provider:  
`Clearlink\Seeder\SeederServiceProvider::class`

Step 4: In your project's config/database.php, add the following PDO option to all connections used for seeding:  
`'options' => [
      PDO::MYSQL_ATTR_LOCAL_INFILE => true  
  ]`

## Useful Tips ##
* The CSV files should have linefeeds, not carriage returns